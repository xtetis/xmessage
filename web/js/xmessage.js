class xmessage {

    /**
     * 
     * @param {*} id_user 
     */
    static validateDataToShowModal(id_user) {
        // Проставляем ID пользователя, с которым планируется открыть список сообщений
        $('#form_xmessage_starter_id_user').val(id_user);
        // Отправляем форму для валидации данных
        $('#form_xmessage_starter').submit();
    }

    /**
     * ПОлучает уникальный урл переписки с указанным пользователем
     * и после получения урла - открывает модалку
     */
    static showModal() {
        console.log(xform.getValidateResponse());
        xform_response = xform.getValidateResponse()

        $('#message_list_page').attr('src', xform_response.data.url_message_list);
        $('#form_message_send').attr('url_validate', xform_response.data.url_ajax_send_message_validate);
        $('.modal__message_box .message_box_user_to').html(xform_response.data.login);
        $('#form_message_send textarea').val('');
        $('.modal__message_box').modal({ backdrop: 'static', keyboard: false, show: true });
    }




    /**
     * Событие после успешной отправки сообщения
     */
    static sendMessageSuccess() {
        xmessage.validateDataToShowModal(xform_response.data.id_user);
    }

    /**
     * Скролл фрейма вних
     */
    static scrollIframeBottom() {
        var $contents = $('#message_list_page').contents();
        $contents.scrollTop($contents.height());
    }
}

function scrollIframeBottom() {
    xmessage.scrollIframeBottom();
}


$(function() {

    /**
     * Нажатие на кнопку показа модалки для личных сообщений
     */
    $(document.body).on('click', '.btn__show_xmessage_modal', function() {
        var idx = $(this).attr('idx');
        idx = Number(idx);
        xmessage.validateDataToShowModal(idx);
    });
});