<?php

namespace xtetis\xmessage;

global $inserted_message_box;

/**
 * Модуль страницы с сообщением для пользователя с единым урлом
 */
class Component extends \xtetis\xengine\models\Component
{

    /**
     * Возвращает блок с модалкой для сообщения между пользователями
     */
    public static function getBlockMessageBox()
    {
        global $inserted_message_box;

        if ((isset($inserted_message_box)) && ($inserted_message_box))
        {
            return '';
        }

        $url_validate_form = self::makeUrl([
            'path' => [
                'list',
                'ajax_validate_form_starter',
            ],
        ]);

        $inserted_message_box = true;

        return self::renderBlock(
            'block/message_box',
            [
                'url_validate_form' => $url_validate_form,
            ]
        );
    }

    /**
     * Возвращает блок с кнопкой для отправки сообщений
     */
    public static function getButtonSendMessage($id_iser = 0)
    {
        $id_iser = intval($id_iser);
        $html = '    <div class="text-center">
        <a href="javascript:void(0)"
           idx="'.$id_iser.'"
           class="btn  btn-warning btn__show_xmessage_modal">
            Отправить сообщение
        </a>
    </div>'.\xtetis\xmessage\Component::getBlockMessageBox();

    

        return $html;
    }



    /**
     * Возвращает количество непрочитанных сообщений для 
     * текущего пользователя (всего, от всех других пользователей)
     */
    public static function getCountUnreadedMessages()
    {
        if (!\xtetis\xuser\Component::isLoggedIn())
        {
            return 0;
        }
        
        $model_current_user = \xtetis\xuser\Component::isLoggedIn();
        
        $model_message_list = new \xtetis\xmessage\models\MessageListModel([
            'id_user_current' => $model_current_user->id,
        ]);
        
        // Возвращает количество сообщений для пользователя (всего, от всех пользователей)
        $model_message_list->getMessageUnreadedAllCount();

        if ($model_message_list->getErrors())
        {
            return 0;
        }
        else
        {
            return $model_message_list->message_unreaded_from_all_count;
        }
    }

}
