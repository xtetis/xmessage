<?php

/**
 * Валидация при добавлении статьи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

if (!\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Только для авторизированных пользователей';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$user_model = \xtetis\xuser\Component::isLoggedIn();

$id_user = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$message = \xtetis\xengine\helpers\RequestHelper::post('message', 'str', '');

$response['js_success'] = 'xmessage.sendMessageSuccess();';
$response['js_fail']    = '';

$params = [
    'id_user_from' => $user_model->id,
    'id_user_to'   => $id_user,
    'message'      => $message,
];

$model_message = new \xtetis\xmessage\models\MessageModel($params);
if ($model_message->sendMessage())
{

    $url = self::makeUrl([
        'path'  => [
            'list',
            'index',
            $id_user,
        ],
        'query' => [
            'uid' => uniqid(),
        ],
    ]);

    $response['result']            = true;
    $response['data']['url_message_list'] = $url;
    $response['data']['id_user'] = $id_user;
}
else
{
    $response['errors'] = $model_message->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
