<?php

/**
 * Рендер списка сообщений
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id_user = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);
if ($page < 1)
{
    $page = 1;
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$model_current_user = \xtetis\xuser\Component::isLoggedIn();

$model_message_list = new \xtetis\xmessage\models\MessageListModel([
    'id_user_current'  => $model_current_user->id,
    'id_user_opponent' => $id_user,
    'offset'           => (20 * ($page - 1)),
]);

$model_message_list->getMessageList();

$url_messages_list = self::makeUrl([
    'path'  => [
        'list',
        'index',
        $id_user,
    ],
    'query' => [
        'uid' => uniqid(),
    ],
]);

$paginate = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $model_message_list->limit,
    $page,
    $model_message_list->message_count,
    $url_messages_list
);

// Рендерим текущую страницу

$page_content = \xtetis\xmessage\Component::renderBlock(
    'list/index',
    [
        'id_user'            => $id_user,
        'model_message_list' => $model_message_list,
        'paginate'           => $paginate,
    ]
);
echo \xtetis\xmessage\Component::renderBlock(
    'layout/message_list',
    [
        'content' => $page_content,
    ]
);
exit;
