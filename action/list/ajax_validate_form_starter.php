<?php

/**
 * Валидация при добавлении категории
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

if (!\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Только для авторизированных пользователей';

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$id_user = \xtetis\xengine\helpers\RequestHelper::post('id_user', 'int', 0);

// Проверяем существование пользователя
$model_user = new \xtetis\xuser\models\UserModel([
    'id' => $id_user,
]);
$model_user->getById();
if ($model_user->getErrors())
{

    $response['errors']['common'] = $model_user->getLastErrorMessage();

    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$url_message_list = self::makeUrl([
    'path'  => [
        'list',
        'index',
        $id_user,
    ],
    'query' => [
        'uid' => uniqid(),
    ],
]);

$url_ajax_send_message_validate = self::makeUrl([
    'path' => [
        'list',
        'ajax_send_message',
        $id_user,
    ],
]);

$response['js_success']                             = 'xmessage.showModal();';
$response['js_fail']                                = '';
$response['result']                                 = true;
$response['data']['url_message_list']               = $url_message_list;
$response['data']['url_ajax_send_message_validate'] = $url_ajax_send_message_validate;
$response['data']['login']                          = $model_user->getUserLoginOrName();

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);

/*

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id_user = \xtetis\xengine\helpers\RequestHelper::post('id', 'int', 0);

$response['url_message_list'] = $url_message_list;
$response['url_validate_form'] = $url_validate_form;

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
*/
