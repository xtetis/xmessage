<?php

/**
 * Рендер списка пользоватей, с которыми есть мообщения
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);
if ($page < 1)
{
    $page = 1;
}

if (!\xtetis\xuser\Component::isLoggedIn())
{
    \xtetis\xengine\helpers\LogHelper::customDie('Только для авторизированных пользователей');
}

$model_current_user = \xtetis\xuser\Component::isLoggedIn();

$model_message_list = new \xtetis\xmessage\models\MessageListModel([
    'id_user_current' => $model_current_user->id,
    'offset'          => (20 * ($page - 1)),
]);

$model_message_list->getUserInfoList();

if ($model_message_list->getErrors())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_message_list->getLastErrorMessage());
}

$url_messages_list = self::makeUrl([
    'path' => [
        'default',
        'index',
    ],
]);

$paginate = \xtetis\xengine\helpers\PaginateHelper::getPagination(
    $model_message_list->limit,
    $page,
    $model_message_list->message_user_count,
    $url_messages_list
);

$list_model_user        = [];
$list_url_user_messages = [];
$list_count_messages    = [];

foreach ($model_message_list->message_user_list as $row)
{
    $model_user = new \xtetis\xuser\models\UserModel([
        'id' => $row['id_user'],
    ]);

    $model_user->getById();

    if ($model_user->getErrors())
    {
        \xtetis\xengine\helpers\LogHelper::customDie($model_user->getLastErrorMessage());
    }

    $list_model_user[$row['id_user']] = $model_user;

    $list_url_user_messages[$row['id_user']] = self::makeUrl([
        'path' => [
            'default',
            'user_messages',
            $row['id_user'],
        ],
    ]);

    $model_message_list_count = new \xtetis\xmessage\models\MessageListModel([
        'id_user_current'  => $model_current_user->id,
        'id_user_opponent' => $row['id_user'],
    ]);

    // Получает количество мообщений между двумя пользователями
    $model_message_list_count->getMessageCount();

    // Получает количество непрочитанных сообщений между двумя пользователями
    $model_message_list_count->getMessageUnreadedCount();

    if ($model_message_list_count->getErrors())
    {
        \xtetis\xengine\helpers\LogHelper::customDie($model_message_list_count->getLastErrorMessage());
    }
    $list_count_messages[$row['id_user']]['all'] = $model_message_list_count->message_count;
    $list_count_messages[$row['id_user']]['unreaded'] = $model_message_list_count->message_unreaded_count;

}

$url_messages = self::makeUrl();

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'paginate'               => $paginate,
        'model_message_list'     => $model_message_list,
        'list_model_user'        => $list_model_user,
        'list_url_user_messages' => $list_url_user_messages,
        'url_messages'           => $url_messages,
        'list_count_messages'    => $list_count_messages,
    ],
);
