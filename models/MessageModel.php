<?php

namespace xtetis\xmessage\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class MessageModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Имя обслуживаемой таблицы
     */
    public $table_name = 'xmessage_message';

    /**
     *ID
     */
    public $id = '';

    /**
     * Кто послал сообщение
     */
    public $id_user_from = '';

    /**
     * Кому сообщение
     */
    public $id_user_to = '';

    /**
     * Текст сообщения
     */
    public $message = '';

    /**
     * Дата создания сообщения
     */
    public $create_date = '';


    /**
     * Добавляет сообщение
     */
    public function sendMessage()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user_from = intval($this->id_user_from);
        $this->id_user_to = intval($this->id_user_to);
        $this->message = strval($this->message);
        $this->message = trim($this->message);

        if (!$this->id_user_from)
        {
            $this->addError('id_user_from', 'Не указан аттрибут id_user_from');

            return false;
        }

        if (!$this->id_user_to)
        {
            $this->addError('id_user_to', 'Не указан аттрибут id_user_to');

            return false;
        }

        if (!strlen($this->message))
        {
            $this->addError('message', 'Не заполнено сообщение');

            return false;
        }

        if (strip_tags($this->message)!=$this->message)
        {
            $this->addError('message', 'Не допустимы HTML теги в сообщениях');

            return false;
        }

        if (mb_strlen($this->message)>300)
        {
            $this->addError('message', 'Максимальная длина сообщения 300 символов');

            return false;
        }

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

}
