<?php

namespace xtetis\xmessage\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class MessageListModel extends \xtetis\xengine\models\Model
{

    /**
     * ID текущего пользователя
     */
    public $id_user_current = 0;

    /**
     * Кому сообщение
     */
    public $id_user_opponent = 0;

    public $limit = 20;

    public $offset = 0;

    /**
     * Количество сообщений между двумя пользователями
     */
    public $message_count = 0;

    /**
     * Количество непротанных сообщений между двумя пользователями
     */
    public $message_unreaded_count = 0;

    /**
     * Количество непрочитанных сообщений от всех пользователей
     */
    public $message_unreaded_from_all_count = 0;

    public $message_list = [];

    /**
     * Список пользователей, у которых есть сообжения
     * от текущего пользователя или текущему пользоваателю
     */
    public $message_user_list = [];
    /**
     * Количество пользователей, у которых есть сообжения
     * от текущего пользователя или текущему пользоваателю
     */
    public $message_user_count = 0;

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        foreach ($params as $k => $v)
        {
            if (property_exists($this, $k))
            {
                $this->$k = $v;
            }
        }
    }

    /**
     * Получает количество мообщений между двумя пользователями
     */
    public function getMessageCount()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user_current = intval($this->id_user_current);
        $this->id_user_opponent = intval($this->id_user_opponent);

        // Количество сообщений между двумя пользователями
        $this->result_sql['getMessageCount'] = \xtetis\xmessage\models\SqlModel::getMessageCount(
            $this->id_user_current ,
            $this->id_user_opponent
        );

        if (!$this->result_sql['getMessageCount'])
        {
            $this->addError('message_count', 'Ошибка при получении количества сообщений');

            return false;
        }

        $this->message_count = $this->result_sql['getMessageCount']['count'];

        return true;

    }




    /**
     * Получает количество непрочитанных сообщений между двумя пользователями
     */
    public function getMessageUnreadedCount()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user_current = intval($this->id_user_current);
        $this->id_user_opponent = intval($this->id_user_opponent);

        // Количество непрочитанных сообщений между двумя пользователями
        $this->result_sql['getMessageUnreadedCount'] = \xtetis\xmessage\models\SqlModel::getMessageUnreadedCount(
            $this->id_user_current ,
            $this->id_user_opponent
        );

        if (!$this->result_sql['getMessageUnreadedCount'])
        {
            $this->addError('message_count', 'Ошибка при получении количества непрочитанных сообщений');

            return false;
        }

        $this->message_unreaded_count = $this->result_sql['getMessageUnreadedCount']['count'];

        return true;

    }

    public function getMessageList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user_current = intval($this->id_user_current);
        $this->id_user_opponent = intval($this->id_user_opponent);
        $this->limit = intval($this->limit);
        $this->offset = intval($this->offset);


        if (!$this->getMessageCount())
        {
            return false;
        }

        $this->message_list = [];



        if ($this->message_count)
        {
            // Получаем список сообщений
            $this->result_sql['getMessageList'] = \xtetis\xmessage\models\SqlModel::getMessageList(
                $this->id_user_current ,
                $this->id_user_opponent,
                $this->limit,
                $this->offset
            );

            
            $this->message_list = array_reverse($this->result_sql['getMessageList']);

            // Проставляем признак того, что сообщения прочитаны
            $this->result_sql['setMessagesReaded'] = \xtetis\xmessage\models\SqlModel::setMessagesReaded(
                $this->id_user_current ,
                $this->id_user_opponent,
                $this->limit,
                $this->offset
            );

        }

        return true;


    }


    /**
     * Возвращает список пользователей, с которыми 
     * есть сообщения у текущего пользователя
     */
    public function getUserInfoList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user_current = intval($this->id_user_current);
        $this->limit = intval($this->limit);
        $this->offset = intval($this->offset);

        // Количество пользователей, с которыми есть сообщения
        $this->result_sql['getMessageUserCount'] = \xtetis\xmessage\models\SqlModel::getMessageUserCount(
            $this->id_user_current
        );

        if (!$this->result_sql['getMessageUserCount'])
        {
            $this->addError('message_user_count', 'Ошибка при получении количества пользователей');

            return false;
        }

        $this->message_user_list = [];

        $this->message_user_count = $this->result_sql['getMessageUserCount']['count'];

        if ($this->message_user_count)
        {
            // Список пользователей, между которыми есть сообщения
            $this->result_sql['getMessageUserList'] = \xtetis\xmessage\models\SqlModel::getMessageUserList(
                $this->id_user_current ,
                $this->limit,
                $this->offset
            );

            
            $this->message_user_list = $this->result_sql['getMessageUserList'];
        }

        return true;
    }

    /**
     * Возвращает количество сообщений для пользователя  (всего, от всех пользователей)
     */
    public function getMessageUnreadedAllCount()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_user_current = intval($this->id_user_current);

        // Количество непрочитанных сообщений для конкретного пользователя (всего, от всех пользователей)
        $this->result_sql['getMessageUnreadedAllCount'] = \xtetis\xmessage\models\SqlModel::getMessageUnreadedAllCount(
            $this->id_user_current
        );

        if (!$this->result_sql['getMessageUnreadedAllCount'])
        {
            $this->addError('message_count', 'Ошибка при получении количества всего непрочитанных сообщений');

            return false;
        }

        $this->message_unreaded_from_all_count = $this->result_sql['getMessageUnreadedAllCount']['count'];

        return true;
    }

}
