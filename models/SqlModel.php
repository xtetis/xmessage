<?php

namespace xtetis\xmessage\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{


    /**
     * Возвращает список сообщений между двумя пользователями
     */
    public static function getMessageList(
        $id_user_current = 0,
        $id_user_to = 0,
        $limit = 0,
        $offset = 0
    )
    {
        $id_user_current = intval($id_user_current);
        $id_user_to = intval($id_user_to);
        $limit = intval($limit);
        $offset = intval($offset);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
            SELECT
                m.*
            FROM 
                xmessage_message m
            WHERE
                (
                    m.id_user_from = :id_user_current
                    AND 
                    m.id_user_to = :id_user_to
                )
                OR 
                (
                    m.id_user_from = :id_user_to
                    AND 
                    m.id_user_to = :id_user_current
                )
            ORDER BY 
                m.create_date  DESC
            LIMIT 
                ".$limit."
            OFFSET 
                ".$offset."
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_current', $id_user_current, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('id_user_to', $id_user_to, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }



    /**
     * ПРоставляет признак того, что сообщения прочитаны
     */
    public static function setMessagesReaded(
        $id_user_current = 0,
        $id_user_to = 0,
        $limit = 0,
        $offset = 0
    )
    {
        $id_user_current = intval($id_user_current);
        $id_user_to = intval($id_user_to);
        $limit = intval($limit);
        $offset = intval($offset);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
            UPDATE xmessage_message
            SET readed=1
            WHERE id IN
            (
                SELECT 
                    * 
                FROM 
                    (
                        SELECT
                            m.id
                        FROM 
                            xmessage_message m
                        WHERE
                            (
                                m.id_user_from = :id_user_to
                                AND 
                                m.id_user_to = :id_user_current
                            )
                        ORDER BY 
                            m.create_date  DESC
                        LIMIT 
                            ".$limit."
                        OFFSET 
                            ".$offset."
                    ) tmp
            )
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_current', $id_user_current, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('id_user_to', $id_user_to, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        // Удаляем кеш согласно тегу
        $cache = \xtetis\xengine\libraries\Cache::getCacheObj();
        $cache->deleteByTag('xmessage_message');

        return $rows;
    }


    /**
     * Возвращает количество сообщений между двумя пользователями
     */
    public static function getMessageCount(
        $id_user_current = 0,
        $id_user_to = 0
    )
    {
        $id_user_current = intval($id_user_current);
        $id_user_to = intval($id_user_to);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
            SELECT
                COUNT(*) as count
            FROM 
                xmessage_message m
            WHERE
                (
                    m.id_user_from = :id_user_current
                    AND 
                    m.id_user_to = :id_user_to
                )
                OR 
                (
                    m.id_user_from = :id_user_to
                    AND 
                    m.id_user_to = :id_user_current
                )
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_current', $id_user_current, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('id_user_to', $id_user_to, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }


    /**
     * Возвращает количество непрочитанных сообщений между двумя пользователями
     */
    public static function getMessageUnreadedCount(
        $id_user_current = 0,
        $id_user_to = 0
    )
    {
        $id_user_current = intval($id_user_current);
        $id_user_to = intval($id_user_to);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
            SELECT
                COUNT(*) as count
            FROM 
                xmessage_message m
            WHERE
                (
                    m.id_user_from = :id_user_to
                    AND 
                    m.id_user_to = :id_user_current
                )
                AND
                m.readed = 0
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_current', $id_user_current, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('id_user_to', $id_user_to, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }


    /**
     * Возвращает количество непрочитанных сообщений для конкретного пользователя 
     * (всего, от всех пользователей)
     */
    public static function getMessageUnreadedAllCount(
        $id_user_current = 0
    )
    {
        $id_user_current = intval($id_user_current);
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
            SELECT
                COUNT(*) as count
            FROM 
                xmessage_message m
            WHERE
                m.id_user_to = :id_user_current
                AND
                m.readed = 0
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_current', $id_user_current, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }

    
    /**
     * Возвращает Количество пользователей, с которыми есть сообщения
     */
    public static function getMessageUserCount(
        $id_user_from = 0
    )
    {
        $id_user_from = intval($id_user_from);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "

            SELECT 
                COUNT(*) as count 
            FROM 
            (
                SELECT DISTINCT 
                    id_user
                FROM 
                (
                    SELECT DISTINCT
                        m.id_user_to as id_user
                    FROM 
                        xmessage_message m
                    WHERE
                        m.id_user_from = :id_user_from
                    
                    UNION
                    
                    SELECT DISTINCT
                        m.id_user_from as id_user
                    FROM 
                        xmessage_message m
                    WHERE
                        m.id_user_to = :id_user_from
                ) tmp 
            ) tmp1
            INNER JOIN 
                xuser_user xu 
                ON xu.id  = tmp1.id_user
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_from', $id_user_from, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }


    /**
     * Возвращает список пользователей, между которыми есть сообщения
     */
    public static function getMessageUserList(
        $id_user_from = 0,
        $limit = 0,
        $offset = 0
    )
    {
        $id_user_from = intval($id_user_from);
        $limit = intval($limit);
        $offset = intval($offset);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $sql = "
            SELECT 
                tmp1.*
            FROM 
            (
                SELECT 
                    id_user,
                    MAX(create_date) as create_date
                FROM 
                (
                    SELECT 
                        xm1.id_user_to as id_user,
                        xm1.create_date 
                    FROM 
                        xmessage_message xm1
                    WHERE
                        xm1.id_user_from = :id_user_from
                        
                    UNION
                    
                    SELECT
                        xm2.id_user_from as id_user,
                        xm2.create_date 
                    FROM 
                        xmessage_message xm2
                    WHERE
                        xm2.id_user_to = :id_user_from
                ) tmp
                GROUP BY 
                    tmp.id_user
            ) tmp1
            INNER JOIN 
                xuser_user xu ON
                xu.id  = tmp1.id_user
    
            LIMIT 
                ".$limit."
            OFFSET 
                ".$offset."
        ";
        $stmt    = $connect->prepare($sql);

        $stmt->bindParam('id_user_from', $id_user_from, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        return $rows;
    }
}
