<?php


// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



?>
<!doctype html>
<html lang="en">
    <?=\xtetis\xengine\helpers\RenderHelper::renderBlock('blocks/head',[]); ?>
    <body>
        <?=$content?>
        <?=\xtetis\xengine\helpers\RenderHelper::renderBlock('blocks/footer',[]); ?>
    </body>
</html>
