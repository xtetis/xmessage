<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки страницы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xmessage.js');

?>
<style>
.pagination_container .pagination {
    margin-left: auto;
    margin-right: auto;
    max-width: fit-content;
}

</style>
<div class="message_list">
    <?php if ($model_message_list->message_list): ?>
<?php foreach ($model_message_list->message_list as $key => $message_info): ?>
<?php if ($id_user == $message_info['id_user_from']): ?>
    <div class="">
        <div style="max-width: fit-content; font-size: 10px; color: gray;">
            <?=@date('d.m.Y H:m:s', strtotime(strval($message_info['create_date'])))?>
        </div>
        <div class="alert alert-success"
             style="max-width: fit-content;"
             role="alert">
            <?=$message_info['message']?>
        </div>
    </div>

    <?php else: ?>
    <div class="">
        <div style="max-width: fit-content; margin-right: 0; margin-left: auto;  font-size: 10px; color: gray;">
            <?=@date('d.m.Y H:m:s', strtotime(strval($message_info['create_date'])))?>
        </div>
        <div class="alert alert-warning"
             style="max-width: fit-content; margin-right: 0; margin-left: auto;"
             role="alert">
            <?=$message_info['message']?>
        </div>
    </div>
    <?php endif;?>
<?php endforeach;?>
<?php else: ?>
    <div style="text-align: center;
    padding-top: 20px;">
        Пока нет сообщений
    </div>
    <?php endif;?>
</div>

<div class="pagination_container">
    <?=$paginate?>
</div>

<script>
window.parent.scrollIframeBottom();
</script>
