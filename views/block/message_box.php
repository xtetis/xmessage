<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xmessage.js');

?>


<div class="modal fade modal__message_box"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true"
     >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title h4"
                    id="myLargeModalLabel">Сообщение пользователю <span class="message_box_user_to"></span></h5>
                <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body"
                 style="    padding: 0.25rem;">
                <div>
                    <iframe src=""
                            style="width:100%;     min-height: calc(80vh - 13em);"
                            id="message_list_page"
                            frameborder="0"></iframe>
                </div>
            </div>
            <div class="modal-footer text-center"
                 style="    padding: 5px;">

                <div style="width: 100%;">
                    <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => '/',
    'form_id'      => 'form_message_send',
    'form_type'    => 'ajax',
]);?>
                    <?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'name' => 'message',
        ],
    ]
)?>

                    <button type="submit"
                            class="btn btn-primary">Отправить сообщение</button>
                    <button type="button"
                            class="btn btn-secondary"
                            data-dismiss="modal">Закрыть</button>
                    <?=\xtetis\xform\Component::renderFormEnd();?>
                </div>



            </div>
        </div>
    </div>
</div>

<div style="display:none"
     id="message_box_starter">

    <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_validate_form,
    'form_id'      => 'form_xmessage_starter',
    'form_type'    => 'ajax',
]);?>
    <input type="hidden" value="0" name="id_user" id="form_xmessage_starter_id_user">

    <?=\xtetis\xform\Component::renderFormEnd();?>
</div>
