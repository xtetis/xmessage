<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки страницы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xmessage.js');


    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Cообщения пользователей',
        ],
    ]);

    
?>
<style>
.pagination_container .pagination {
    margin-left: auto;
    margin-right: auto;
    max-width: fit-content;
}

</style>
<div class="message_list">
    <?php foreach ($model_message_list->message_user_list as $row): ?>
    <a href="javascript:void(0)"
       class="btn__show_xmessage_modal"
       idx="<?=$row['id_user']?>">
        <div class="alert alert-success"
             style=""
             role="alert">
            <div class="row">
                <div class="col-sm">
                    <img src="/assets/images/user/unnamed_user_logo.png"
                         alt=""
                         style="max-width: 70px;"
                         srcset="">
                    <span class="badge badge-pill badge-success"
                          style="font-size: 26px;">
                        <?=$list_model_user[$row['id_user']]->getUserLoginOrName()?>
                    </span>
                </div>
                <div class="col-sm">
                    <div>
                        Всего сообщений:
                        <span class="badge badge-pill badge-warning">
                            <?=$list_count_messages[$row['id_user']]['all']?>
                        </span>
                    </div>
                    <div>
                        Последнее сообщение:
                        <span class="badge badge-pill badge-warning">
                            <?=@date('d.m.Y H:m:s', strtotime(strval($row['create_date'])))?>
                        </span>
                    </div>
                    <div>
                        Непрочитанных:
                        <span class="badge badge-pill badge-dark">
                            <?=$list_count_messages[$row['id_user']]['unreaded']?>
                        </span>
                    </div>
                    <div>
                    <button type="button" class="btn  btn-warning" >
                        Отправить сообщение
                    </button>
                    </div>
                </div>
            </div>
        </div>
    </a>
    <?php endforeach; ?>
</div>

<div class="pagination_container">
    <?=$paginate?>
</div>
<?=\xtetis\xmessage\Component::getBlockMessageBox()?>
